from django.contrib.auth.models import User
from django.db import models


# Create your models here.
class Product(models.Model):
    Title_of_product = models.CharField(max_length=100)
    Description_Product = models.TextField(max_length=150)
    Image_Link = models.URLField()
    Price_of_Product = models.DecimalField(max_digits=8,decimal_places=4)
    Created_at = models.DateField(auto_now_add=True)
    Updated_at = models.DateField(auto_now=True)

    def __str__(self):
        return self.Title_of_product


STATUS_CHOICES = (
    ('new', 'new'),
    ('paid', 'paid')
)

MODE_OF_PAYMENT = (
    ('cash_on_delivery', 'cash_on_delivery'),

)


class Orders(models.Model):
    User = models.ForeignKey(User, on_delete=models.CASCADE)
    Total_order= models.IntegerField()
    Created_at = models.DateField(auto_now_add=True)
    Updated_at = models.DateField(auto_now=True)
    Status_of_order = models.CharField(choices=STATUS_CHOICES, max_length=5)
    Mode_of_payment = models.CharField(choices=MODE_OF_PAYMENT, max_length=16)
    Product_Order = models.ForeignKey(Product, on_delete=models.CASCADE)

    def __str__(self):
        return self.Product.Title_of_product


class OrderItems(models.Model):
    Order_Id = models.ForeignKey(Orders, on_delete=models.CASCADE)
    Product_Id = models.ForeignKey(Product, on_delete=models.CASCADE)
    Quantity_of_item = models.IntegerField()
    Price_of_product = models.FloatField()
    User = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.ProductId.Title_of_product
from django.db import models

# Create your models here.
