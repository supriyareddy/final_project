from rest_framework import viewsets
from rest_framework import permissions
from rest_framework.exceptions import PermissionDenied
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.renderers import JSONRenderer, TemplateHTMLRenderer

from cart1.models import Product, Orders, OrderItems
from cart1.serializers import ProductSerializer, OrdersSerializer, OrderItemsSerializer


class IsUser(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        return obj.UserId == request.user


class ProductList(viewsets.ReadOnlyModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


class ProductDetails(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    search_fields = ['id', 'Title']
    filter_backends = (SearchFilter, OrderingFilter)


class OrderList(viewsets.ReadOnlyModelViewSet):
    serializer_class = OrdersSerializer
    permission_classes = (IsUser,)


    def get_queryset(self):
        user = self.request.user
        if user.is_authenticated:
            return Orders.objects.filter(User=user)
        raise PermissionDenied()

    def perform_create(self, serializer):
        serializer.save(User=self.request.user)


class OrderDetails(viewsets.ModelViewSet):
    serializer_class = OrdersSerializer
    permission_classes = (IsUser,)

    def get_queryset(self):
        user = self.request.user
        if user.is_authenticated:
            return Orders.objects.filter(User=user)
        raise PermissionDenied()

    def perform_create(self, serializer):
        serializer.save(User=self.request.user)


class OrderItemList(viewsets.ReadOnlyModelViewSet):
    serializer_class = OrderItemsSerializer
    permission_classes = (IsUser,)

    def get_queryset(self):
        user = self.request.user
        if user.is_authenticated:
            return OrderItems.objects.filter(User=user)
        raise PermissionDenied()

    def perform_create(self, serializer):
        serializer.save(User=self.request.user)


class OrderItemDetails(viewsets.ModelViewSet):
    serializer_class = OrderItemsSerializer
    permission_classes = (IsUser,)

    def get_queryset(self):
        user = self.request.user
        if user.is_authenticated:
            return OrderItems.objects.filter(User=user)
        raise PermissionDenied()

    def perform_create(self, serializer):
        serializer.save(User=self.request.user)
